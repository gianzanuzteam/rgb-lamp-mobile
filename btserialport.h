#ifndef BTSERIALPORT_H
#define BTSERIALPORT_H

#include <QObject>

#include <QBluetoothLocalDevice>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothServiceInfo>
#include <QBluetoothSocket>

class BTSerialPort : public QObject
{
    Q_OBJECT

public:

    enum BT_STATE{
        BT_INIT = 0,
        BT_ADAPTER_FOUND,
        BT_ADAPTER_NOT_FOUND,
        BT_DEVICE_SEARCH,
        BT_DEVICE_FOUND,
        BT_DEVICE_NOT_FOUND,
        BT_DEVICE_DISCONNECTED,
        BT_DEVICE_CONNECTING,
        BT_DEVICE_CONNECTED,
        BT_ERROR
    };

    explicit BTSerialPort(QObject *parent = 0);
    void setupBluetooth(void);

    QBluetoothSocket* socket{};


private slots:
    void deviceDiscovered(const QBluetoothDeviceInfo &device);
    void deviceDiscoveredFinished(void);
    void socketStateChanged(QBluetoothSocket::SocketState state);
    void socketError();

signals:
    void bt_state_valueChanged(BTSerialPort::BT_STATE state);

private:
    QString deviceName = "RGB";
    QBluetoothLocalDevice localDevice;
    QBluetoothDeviceDiscoveryAgent* discoveryAgent{};
    BT_STATE bt_state = BT_INIT;

    void connectDevice(const QBluetoothDeviceInfo &device);
};

#endif // BTSERIALPORT_H
