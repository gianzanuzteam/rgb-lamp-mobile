#include "btserialport.h"

BTSerialPort::BTSerialPort(QObject *parent) : QObject(parent)
{

}

void BTSerialPort::setupBluetooth(void)
{
    /* Verifica se o dispositivo possui adaptador bluetooth */
    if (localDevice.isValid())
    {
        /* MUDANÇA DE ESTADO */
        bt_state = BT_ADAPTER_FOUND;
        emit bt_state_valueChanged(bt_state);

        /* Liga o bluetooth */
        localDevice.powerOn();

        /* MUDANÇA DE ESTADO */
        bt_state = BT_DEVICE_SEARCH;
        emit bt_state_valueChanged(bt_state);

        /* Inicia descobrimento por dispositivos */
        /* Cria um agente de descobrimento */
        /* Cria sinal para tratar novo descobrimento */
        if(discoveryAgent)
            delete discoveryAgent;
        discoveryAgent = new QBluetoothDeviceDiscoveryAgent(this);
        connect(discoveryAgent, SIGNAL(deviceDiscovered(QBluetoothDeviceInfo)), this, SLOT(deviceDiscovered(QBluetoothDeviceInfo)));
        connect(discoveryAgent, SIGNAL(finished()), this, SLOT(deviceDiscoveredFinished()));
        discoveryAgent->start();
    }
    else
    {
        /* MUDANÇA DE ESTADO */
        bt_state = BT_ADAPTER_NOT_FOUND;
        emit bt_state_valueChanged(bt_state);
    }
}

/* Sinal para tratar novo dispositivo encontrado */
void BTSerialPort::deviceDiscovered(const QBluetoothDeviceInfo &device)
{
    /* Caso encontre o dispositivo, encerra o descobrimento e inicia a conexão */
    if(device.name().contains(deviceName, Qt::CaseSensitive))
    {
        /* MUDANÇA DE ESTADO */
        bt_state = BT_DEVICE_FOUND;
        emit bt_state_valueChanged(bt_state);

        discoveryAgent->stop();
        discoveryAgent->deleteLater();
        connectDevice(device);
    }
}

/* Sinal para tratar quando o dispositivo não foi encontrado */
void BTSerialPort::deviceDiscoveredFinished(void)
{
    /* MUDANÇA DE ESTADO */
    bt_state = BT_DEVICE_NOT_FOUND;
    emit bt_state_valueChanged(bt_state);

    discoveryAgent->deleteLater();
}

/* Conecta ao dispositivo bluetooth */
void BTSerialPort::connectDevice(const QBluetoothDeviceInfo &device)
{
    /* Verifica se já foi iniciado */
    if(socket)
        delete socket;

    /* Configrua UUID do serviço para SerialPort */
    /* Inicia socket para envio de dados */
    static const QLatin1String serviceUuid("00001101-0000-1000-8000-00805F9B34FB");
    socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol);
    socket->connectToService(device.address(), QBluetoothUuid(serviceUuid));
    connect(socket, SIGNAL(error(QBluetoothSocket::SocketError)), this, SLOT(socketError()));
    connect(socket, SIGNAL(stateChanged(QBluetoothSocket::SocketState)), this, SLOT(socketStateChanged(QBluetoothSocket::SocketState)));
}

/* Sinal para tratar mudança de status do socket */
void BTSerialPort::socketStateChanged(QBluetoothSocket::SocketState state)
{
    /* MUDANÇA DE ESTADO */
    switch (state)
    {
        case QBluetoothSocket::UnconnectedState:
        {
            bt_state = BT_DEVICE_DISCONNECTED;
        }break;

        case QBluetoothSocket::ConnectingState:
        {
            bt_state = BT_DEVICE_CONNECTING;
        }break;

        case QBluetoothSocket::ConnectedState:
        {
            bt_state = BT_DEVICE_CONNECTED;
        }break;

        default:
        {
            bt_state = BT_ERROR;
        } break;
    }
    emit bt_state_valueChanged(bt_state);
}

/* Sinal para tratar erro no socket */
void BTSerialPort::socketError()
{
    /* MUDANÇA DE ESTADO */
    bt_state = BT_ERROR;
    emit bt_state_valueChanged(bt_state);
}
