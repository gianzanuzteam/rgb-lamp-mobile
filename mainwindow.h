#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFrame>
#include <QMainWindow>
#include <QRotationSensor>
#include <QTimer>
#include "btserialport.h"

//#define ENABLE_SENSORS

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_dial_R_valueChanged(int position);
    void on_dial_G_valueChanged(int position);
    void on_dial_B_valueChanged(int position);
    void bt_state_valueChanged(BTSerialPort::BT_STATE state);
#ifdef ENABLE_SENSORS
    void on_checkBox_clicked(bool checked);
    void updateRotation(void);
#endif

private:
    enum RGB_colours
    {
        RED = 0,
        GREEN,
        BLUE,
        RGB_SIZE
    };

    Ui::MainWindow *ui;
    void setupMainApp(void);
    void mainApp(void);

    /* RGB */
    int RGBcolours[RGB_SIZE]{};
    QString frameStyle = "border: 5px solid black; border-radius: 50px";
    void serialReadData(void);
    void colour_send();

    /* Bluetooth */
    BTSerialPort btSerialPort{};
    void setupSerialPort(void);

#ifdef ENABLE_SENSORS
    /* Acelerômetro */
    QTimer* timer{};
    QRotationSensor* rotation{};
#endif
};

#endif // MAINWINDOW_H
