#-------------------------------------------------
#
# Project created by QtCreator 2016-10-08T10:26:27
#
#-------------------------------------------------

QT       += core gui bluetooth sensors

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RGB_lamp
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    btserialport.cpp

HEADERS  += mainwindow.h \
    btserialport.h

FORMS    += mainwindow.ui

INCLUDEPATH += $$PWD/../../../../Qt/5.7/msvc2015_64/include
DEPENDPATH += $$PWD/../../../../Qt/5.7/msvc2015_64/include

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
