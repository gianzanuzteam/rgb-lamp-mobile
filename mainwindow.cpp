#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupMainApp();
    mainApp();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* Setup inicial dos widgets */
void MainWindow::setupMainApp(void)
{
    /* Limpa variável que contém as cores */
    for(int i=0; i<RGB_SIZE; i++)
        RGBcolours[i] = 128;

    /* Widget display */
    ui->frame_R->setStyleSheet(".QFrame{background-color: red; border: 1px solid black; border-radius: 10px;}");
    ui->frame_G->setStyleSheet(".QFrame{background-color: green; border: 1px solid black; border-radius: 10px;}");
    ui->frame_B->setStyleSheet(".QFrame{background-color: blue; border: 1px solid black; border-radius: 10px;}");
    ui->frame_RGB->setStyleSheet(".QFrame{border: 1px solid black; border-radius: 10px;}");
    ui->frame_R->setAutoFillBackground(true);
    ui->frame_G->setAutoFillBackground(true);
    ui->frame_B->setAutoFillBackground(true);
    ui->frame_RGB->setAutoFillBackground(true);

    /* Colour Dial */
    ui->dial_R->setRange(0, 255);
    ui->dial_G->setRange(0, 255);
    ui->dial_B->setRange(0, 255);
    ui->dial_R->setValue(128);
    ui->dial_G->setValue(128);
    ui->dial_B->setValue(128);

    /* Box de escolha de cor */
    ui->colour_box->setEnabled(false);


#ifdef ENABLE_SENSORS
    ui->checkBox->setVisible(true);
#else
    ui->checkBox->setVisible(false);
#endif

    /* Setup do Bluetooth */
    connect(&btSerialPort, SIGNAL(bt_state_valueChanged(BTSerialPort::BT_STATE)), this, SLOT(bt_state_valueChanged(BTSerialPort::BT_STATE)));
    btSerialPort.setupBluetooth();
}

/* APP principal */
void MainWindow::mainApp(void)
{
    /* Do nothing */
}

/* Envia configuração atual */
void MainWindow::colour_send()
{
    /* Verifica se já foi estabelecido o socket */
    if(!btSerialPort.socket)
        return;

    btSerialPort.socket->write("#SET_COLOUR\n");
    btSerialPort.socket->write(QByteArray::number(-1*(RGBcolours[RED]   - 255)) + "\n"); /* Maracutaia braba para inverter lógica */
    btSerialPort.socket->write(QByteArray::number(-1*(RGBcolours[GREEN] - 255)) + "\n");
    btSerialPort.socket->write(QByteArray::number(-1*(RGBcolours[BLUE]  - 255)) + "\n");
    btSerialPort.socket->write("#END\n");
}

/* Evento: Mudança no valor dos dial */
void MainWindow::on_dial_R_valueChanged(int position)
{
    RGBcolours[RED] = position;
    ui->frame_R->setStyleSheet(QString(".QFrame{background-color: rgb(%1,%2,%3); %4}").arg(RGBcolours[RED]).arg(0).arg(0).arg(frameStyle));
    ui->frame_RGB->setStyleSheet(QString(".QFrame{background-color: rgb(%1,%2,%3); %4}").arg(RGBcolours[RED]).arg(RGBcolours[GREEN]).arg(RGBcolours[BLUE]).arg(frameStyle));
    colour_send();
}

void MainWindow::on_dial_G_valueChanged(int position)
{
    RGBcolours[GREEN] = position;
    ui->frame_G->setStyleSheet(QString(".QFrame{background-color: rgb(%1,%2,%3); %4}").arg(0).arg(RGBcolours[GREEN]).arg(0).arg(frameStyle));
    ui->frame_RGB->setStyleSheet(QString(".QFrame{background-color: rgb(%1,%2,%3); %4}").arg(RGBcolours[RED]).arg(RGBcolours[GREEN]).arg(RGBcolours[BLUE]).arg(frameStyle));
    colour_send();
}

void MainWindow::on_dial_B_valueChanged(int position)
{
    RGBcolours[BLUE] = position;
    ui->frame_B->setStyleSheet(QString(".QFrame{background-color: rgb(%1,%2,%3); %4}").arg(0).arg(0).arg(RGBcolours[BLUE]).arg(frameStyle));
    ui->frame_RGB->setStyleSheet(QString(".QFrame{background-color: rgb(%1,%2,%3); %4}").arg(RGBcolours[RED]).arg(RGBcolours[GREEN]).arg(RGBcolours[BLUE]).arg(frameStyle));
    colour_send();
}

/* Evento: Mundaça de estado do BT */
void MainWindow::bt_state_valueChanged(BTSerialPort::BT_STATE state)
{
    /* Desabilita escolha de cor */
    ui->colour_box->setEnabled(false);

    switch (state) {
    case BTSerialPort::BT_INIT:
        ui->textBrowser->append("Status: Iniciando Bluetooth");
        break;
    case BTSerialPort::BT_ADAPTER_FOUND:
        ui->textBrowser->append("Status: Adaptador Bluetooth encontrado");
        break;
    case BTSerialPort::BT_ADAPTER_NOT_FOUND:
        ui->textBrowser->append("Erro: Adaptador Bluetooth não encontrado");
        break;
    case BTSerialPort::BT_DEVICE_SEARCH:
        ui->textBrowser->append("Status: Iniciando descoberta de dispositivos");
        break;
    case BTSerialPort::BT_DEVICE_FOUND:
        ui->textBrowser->append("Status: Dispositivo encontrado");
        break;
    case BTSerialPort::BT_DEVICE_NOT_FOUND:
        ui->textBrowser->append("Erro: Dispositivo não encontrado");
        break;
    case BTSerialPort::BT_DEVICE_DISCONNECTED:
        ui->textBrowser->append("Status: Dispositivo desconectado");
        break;
    case BTSerialPort::BT_DEVICE_CONNECTING:
        ui->textBrowser->append("Status: Conectando ao dispositivo");
        break;
    case BTSerialPort::BT_DEVICE_CONNECTED:
        ui->colour_box->setEnabled(true);
        ui->textBrowser->append("Status: Conectado ao dispositivo com sucesso");
        break;
    case BTSerialPort::BT_ERROR:
        ui->textBrowser->append("Erro: Ocorreu um erro na conexão Bluetooth");
        break;
    default:
        ui->textBrowser->append("Erro: Não identificado");
        break;
    }
}

#ifdef ENABLE_SENSORS
/* Evento: Checkbox de habilitar acelerômetro */
void MainWindow::on_checkBox_clicked(bool checked)
{
    if(checked)
    {
        rotation = new QRotationSensor();
        rotation->start();

        timer = new QTimer();
        connect(timer, SIGNAL(timeout()), this, SLOT(updateRotation()));
        timer->start(100);
    }
    else
    {
        rotation->stop();
        rotation->deleteLater();

        timer->stop();
        timer->deleteLater();
    }
}

/* Evento: Timeout do timer -> atualizar valores do acelerômetro */
void MainWindow::updateRotation(void)
{
    QRotationReading* rotationReading = rotation->reading();

    ui->dial_R->setValue((quint8) qRound((rotationReading->x() + 90.0)*255.0/180.0));
    ui->dial_G->setValue((quint8) qRound((rotationReading->y() + 180.0)*255.0/360.0));
    ui->dial_B->setValue((quint8) qRound((rotationReading->z() + 180.0)*255.0/360.0));
}
#endif


